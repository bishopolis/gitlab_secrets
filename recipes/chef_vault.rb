
#
# Cookbook Name:: template
# Recipe:: default
# License:: MIT
#
# Copyright 2016, GitLab Inc.
chef_gem 'chef-vault' do # ~FC009
  compile_time true if Chef::Resource::ChefGem.method_defined?(:compile_time)
end
